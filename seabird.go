// Seabird provides a communication interface for Seabird sensors
package seabird

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"io"
	"os"
	"strings"
	"sync"
)

const (
	// Command terminator
	EOL = "\r"
	// Command prompt
	PROMPT = "S>"
	// Data record terminator in streaming mode
	EOR = "\r\n"
)

var BadCommand = errors.New("Invalid command")

type Device struct {
	port     io.ReadWriter
	respBuf  bytes.Buffer
	rmu, wmu *sync.Mutex
	lastCmd  string
}

func NewDevice(port io.ReadWriter) *Device {
	return &Device{
		port: port,
		rmu:  &sync.Mutex{},
		wmu:  &sync.Mutex{}}
}

// Wrap a ReadWriter to read back the characters echoed by the sensor.
type echoCanceler struct {
	rw io.ReadWriter
}

// Write the byte slice p to the sensor read the echoed characters.
func (ec *echoCanceler) Write(p []byte) (int, error) {
	echo := make([]byte, 1)
	for i := 0; i < len(p); i++ {
		ec.rw.Write(p[i : i+1])
		_, err := ec.rw.Read(echo)
		if err != nil {
			return i, err
		}
		if echo[0] == '\r' {
			// Wait for linefeed
			_, err := ec.rw.Read(echo)
			if err != nil {
				return i, err
			}
		}
	}
	return len(p), nil
}

func (ec *echoCanceler) Read(p []byte) (int, error) {
	return ec.rw.Read(p)
}

var _ io.ReadWriter = (*echoCanceler)(nil)

// SetEchoMode informs the Device that the sensor echoes back all characters
// sent to it.
func (d *Device) SetEchoMode(state bool) {
	if state {
		if _, ok := d.port.(*echoCanceler); !ok {
			d.port = &echoCanceler{rw: d.port}
		}
	} else {
		if ec, ok := d.port.(*echoCanceler); ok {
			d.port = ec.rw
		}
	}
}

// EchoMode returns true if the sensor echoes back all characters.
func (d *Device) EchoMode() bool {
	_, ok := d.port.(*echoCanceler)
	return ok
}

func (d *Device) readUntil(marker []byte) (string, error) {
	d.rmu.Lock()
	defer d.rmu.Unlock()

	d.respBuf.Reset()
	tail := -len(marker)
	b := make([]byte, 1)
	for {
		n, err := d.port.Read(b)
		if n == 0 {
			err = os.ErrDeadlineExceeded
		}

		if err != nil {
			return d.respBuf.String(), err
		}

		d.respBuf.Write(b)
		tail++
		if tail < 0 {
			continue
		}

		if bytes.Equal(d.respBuf.Bytes()[tail:], marker) {
			return strings.TrimSuffix(d.respBuf.String(), string(marker)), nil
		}
	}

}

// Send sends a command to the Device
func (d *Device) Send(cmd string) error {
	var err error
	d.wmu.Lock()
	defer d.wmu.Unlock()

	d.lastCmd = cmd
	_, err = fmt.Fprint(d.port, cmd, EOL)
	return err
}

// Recv returns a response from the Device.
func (d *Device) Recv() (string, error) {
	resp, err := d.readUntil([]byte(PROMPT))
	if err != nil {
		return "", err
	}

	if strings.HasSuffix(resp, "?CMD") {
		return resp, fmt.Errorf("%q: %w", d.lastCmd, BadCommand)
	}

	return resp, nil
}

// Exec sends a command to the Device and returns the response along with
// any error that occurs.
func (d *Device) Exec(cmd string) (string, error) {
	err := d.Send(cmd)
	if err != nil {
		return "", err
	}
	return d.Recv()
}

// Start puts the Device into streaming mode
func (d *Device) Start() error {
	err := d.Send("startprofile")
	if err != nil {
		return err
	}
	_, err = d.Recv()
	return err
}

// Stop takes the device out of streaming mode.
func (d *Device) Stop() error {
	err := d.Send("stopprofile")
	if err != nil {
		return err
	}
	_, err = d.Recv()
	return err
}

// Stream returns a channel which will supply data records when the Device
// is in "streaming mode", e.g. after a "startprofile" command has been
// sent.
func (d *Device) Stream(ctx context.Context) <-chan string {
	ch := make(chan string, 1)
	go func() {
		defer close(ch)
		for ctx.Err() == nil {
			text, err := d.readUntil([]byte(EOR))
			if err != nil {
				return
			}
			select {
			case <-ctx.Done():
				return
			case ch <- text:
			default:
			}
		}
	}()

	return ch
}
