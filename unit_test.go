package seabird

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"testing"
)

func TestParse(t *testing.T) {
	table := []struct {
		in, out string
		err     error
	}{
		{
			in:  "\r\ndevice response\r\nS>",
			out: "\r\ndevice response\r\n",
		},
		{
			in:  "\r\n?CMDS>",
			out: "\r\n?CMD",
			err: BadCommand,
		},
	}

	var b bytes.Buffer
	dev := NewDevice(&b)

	for _, e := range table {
		b.Write([]byte(e.in))
		resp, err := dev.Recv()
		if resp != e.out {
			t.Errorf("Bad response; expected %q, got %q", e.out, resp)
		}
		if !errors.Is(err, e.err) {
			t.Errorf("Expected %v, got %v", e.err, err)
		}
	}
}

type pipeRw struct {
	*io.PipeReader
	*io.PipeWriter
}

func TestEchoMode(t *testing.T) {
	r1, w1 := io.Pipe()
	r2, w2 := io.Pipe()

	rw := pipeRw{PipeReader: r1, PipeWriter: w2}
	go func() {
		char := make([]byte, 1)
		for char[0] != '\r' {
			_, err := r2.Read(char)
			if err != nil {
				return
			}
			w1.Write(char)
		}
		fmt.Fprint(w1, "\nok\r\nS>")
	}()

	dev := NewDevice(rw)
	dev.SetEchoMode(true)
	resp, err := dev.Exec("foo")
	if err != nil {
		t.Fatal(err)
	}

	expect := "ok\r\n"
	if resp != expect {
		t.Errorf("Bad response; want %q, got %q", expect, resp)
	}
}

func TestNoEcho(t *testing.T) {
	r1, w1 := io.Pipe()
	r2, w2 := io.Pipe()

	rw := pipeRw{PipeReader: r1, PipeWriter: w2}
	go func() {
		char := make([]byte, 1)
		for char[0] != '\r' {
			_, err := r2.Read(char)
			if err != nil {
				return
			}
		}
		fmt.Fprint(w1, "\r\nok\r\nS>")
	}()

	dev := NewDevice(rw)
	resp, err := dev.Exec("foo")
	if err != nil {
		t.Fatal(err)
	}

	expect := "\r\nok\r\n"
	if resp != expect {
		t.Errorf("Bad response; want %q, got %q", expect, resp)
	}
}

func TestBadCommand(t *testing.T) {
	r1, w1 := io.Pipe()
	r2, w2 := io.Pipe()

	rw := pipeRw{PipeReader: r1, PipeWriter: w2}
	go func() {
		char := make([]byte, 1)
		for char[0] != '\r' {
			_, err := r2.Read(char)
			if err != nil {
				return
			}
		}
		fmt.Fprint(w1, "\r\n?CMDS>")
	}()

	dev := NewDevice(rw)
	_, err := dev.Exec("foo")
	if !errors.Is(err, BadCommand) {
		t.Errorf("Unexpected error value: %v", err)
	}
}
